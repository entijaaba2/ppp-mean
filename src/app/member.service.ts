import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class MemberService {

  constructor(private http:Http) { }

  getMemeber() {
    return this.http.get('http://localhost:3000/api/member').map(res => res.json()).subscribe(data => {
      if (data.success) {
        console.log(data);
        console.log('success');
      }
    }, err => {
      console.log(err);
      console.log('err');
    });
  }

}

import {Component, OnInit} from "@angular/core";
import {MemberService} from "./member.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Site Labo';

  constructor(private memberService:MemberService) {

  }

  ngOnInit() {
  }
}
